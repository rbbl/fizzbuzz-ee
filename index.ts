let fizzBuzzContainer: HTMLElement
let calculationTriggeringSpan: HTMLElement | null = null
let i = 0

window.onload = setup()

function setup(): any {
    fizzBuzzContainer = document.getElementById("fizzbuzz-container")!
    do {
        bulkGenerateElements(true)
    } while (isInViewport(calculationTriggeringSpan!))
    document.onscroll = function () {
        bulkGenerateElements(false)
    }
    window.visualViewport.addEventListener('resize', bulkGenerateElements(false))
}

function bulkGenerateElements(skipCheck: boolean): any {
    if (skipCheck || isInViewport(calculationTriggeringSpan!)) {
        let baseGenerationNumber = i
        for (; i < baseGenerationNumber + 100; i++) {
            if (i != baseGenerationNumber + 50) {
                fizzBuzzAppender(fizzBuzzer(i))
            } else {
                calculationTriggeringSpan = fizzBuzzAppender(fizzBuzzer(i))
            }
        }
    }
}

function fizzBuzzAppender(input: string): HTMLElement {
    let span = document.createElement("span")
    span.innerHTML = input
    fizzBuzzContainer.appendChild(span)
    fizzBuzzContainer.appendChild(document.createElement("br"))
    return span
}

function fizzBuzzer(input: number): string {
    if (input % 15 == 0) {
        return "fizzbuzz"
    }
    if (input % 3 == 0) {
        return "fizz"
    }
    if (input % 5 == 0) {
        return "buzz"
    }
    return String(input)
}

function isInViewport(element: HTMLElement) {
    const rect = element.getBoundingClientRect();
    return (
        rect.top >= 0 &&
        rect.left >= 0 &&
        rect.bottom <= (window.innerHeight || document.documentElement.clientHeight) &&
        rect.right <= (window.innerWidth || document.documentElement.clientWidth)
    );
}
