# FizzBuzz Endless Edition
Just another FizzBuzz Implementation in the Form of a static HTML endless Scroller.

Check out the [live Version](https://rbbl.gitlab.io/fizzbuzz-ee/).

## Requirements
- TypeScript (globally installed)

## Build
```shell
tsc
```